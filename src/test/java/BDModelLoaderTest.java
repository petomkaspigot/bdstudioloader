import de.petomka.bdstudioloader.model.BDComponent;
import de.petomka.bdstudioloader.model.BDComponentType;
import de.petomka.bdstudioloader.model.BDModel;
import de.petomka.bdstudioloader.model.BDModelLoader;
import de.petomka.bdstudioloader.model.components.BDBlockDisplay;
import de.petomka.bdstudioloader.model.components.BDCollection;
import de.petomka.bdstudioloader.model.components.BDItemDisplay;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;

import static org.junit.jupiter.api.Assertions.*;

public class BDModelLoaderTest {

    @Test
    public void testLoadFile() throws IOException {
        BDModel model = BDModelLoader.loadFromFile(new File("src/test/resources/clock.bdstudio"));

        assertNotNull(model, "model was null");
        assertFalse(model.getComponents().isEmpty(), "model is empty");
        assertEquals("Project", model.getName());

        for (BDComponent component : model.getComponents()) {
            testComponent(component);
        }
    }

    private void testComponent(BDComponent component) {
        assertNotNull(component.type(), "component has no type");
        assertNotNull(component.name(), "component has no name");
        assertNotNull(component.transform(), "component has no transform");

        if (component.type() == BDComponentType.BLOCK_DISPLAY) {
            assertTrue(component instanceof BDBlockDisplay, "component reported wrong type");
        }
        if (component.type() == BDComponentType.ITEM_DISPLAY) {
            assertTrue(component instanceof BDItemDisplay, "component reported wrong type");
        }
        if (component.type() == BDComponentType.COLLECTION) {
            assertTrue(component instanceof BDCollection, "component reported wrong type");
            testCollection((BDCollection) component);
        }
    }

    private void testCollection(BDCollection collection) {
        for (BDComponent child : collection.children()) {
            assertNotNull(child);
            testComponent(child);
        }
    }

}
