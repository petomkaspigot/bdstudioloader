package de.petomka.bdstudioloader.commands;

import de.petomka.bdstudioloader.BDStudioLoader;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabExecutor;
import org.bukkit.entity.Player;
import org.bukkit.plugin.PluginBase;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

public class BDStudioLoaderCommand implements TabExecutor {

    private final BDStudioLoader bdStudioLoader;

    public BDStudioLoaderCommand(BDStudioLoader bdStudioLoader) {
        this.bdStudioLoader = bdStudioLoader;
    }

    private static final List<Function<BDStudioLoader, String>> LINES = new ArrayList<>();

    static {
        LINES.add(PluginBase::getName);
        LINES.add((bds) -> "version " + bds.getDescription().getVersion());
        LINES.add((bds) -> "by " + String.join(", ", bds.getDescription().getAuthors()));
        LINES.add((bds) -> "Website: " + bds.getDescription().getWebsite());
        LINES.add((bds) -> "Additional Credit: " + String.join(", ", bds.getDescription().getContributors()));
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (!(sender instanceof Player player)) {
            for (var line : LINES) {
                sender.sendMessage(line.apply(bdStudioLoader));
            }
            return true;
        }
        for (var line : LINES) {
            bdStudioLoader.sendPrimaryMessage(player, line.apply(bdStudioLoader));
        }
        return true;
    }

    @Override
    public List<String> onTabComplete(CommandSender sender, Command command, String label, String[] args) {
        return List.of();
    }

}
