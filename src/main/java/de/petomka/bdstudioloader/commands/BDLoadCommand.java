package de.petomka.bdstudioloader.commands;

import de.petomka.bdstudioloader.BDStudioLoader;
import de.petomka.bdstudioloader.gui.BookModelSelect;
import de.petomka.bdstudioloader.managers.ModelEditManager;
import de.petomka.bdstudioloader.model.BDModel;
import de.petomka.bdstudioloader.model.BDModelLoader;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabExecutor;
import org.bukkit.entity.BlockDisplay;
import org.bukkit.entity.Player;
import org.bukkit.util.RayTraceResult;
import org.joml.Matrix4f;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

public class BDLoadCommand implements TabExecutor {

    private final BDStudioLoader bdStudioLoader;
    private final ModelEditManager modelEditManager;

    public BDLoadCommand(BDStudioLoader bdStudioLoader, ModelEditManager modelEditManager) {
        this.bdStudioLoader = bdStudioLoader;
        this.modelEditManager = modelEditManager;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (!(sender instanceof Player player)) {
            sender.sendMessage("Command is for players only");
            return true;
        }
        if (modelEditManager.isPlacing(player)) {
            bdStudioLoader.sendErrorMessage(player, "Please finish placing your current model (left click to place).");
            return false;
        }
        if (args.length < 1) {
            BookModelSelect.open(bdStudioLoader, player);
            bdStudioLoader.sendErrorMessage(player, "Please specify the object to load!");
            return false;
        }
        String fileName = String.join(" ", args);

        RayTraceResult rayTraceResult = player.rayTraceBlocks(BDStudioLoader.PLACE_DISTANCE);
        if (rayTraceResult == null) {
            bdStudioLoader.sendErrorMessage(player, "You must be looking at a block.");
            return false;
        }

        Location location = rayTraceResult.getHitPosition().toLocation(player.getWorld());
        BDModel bdModel;

        try {
            bdModel = BDModelLoader.loadFromFile(bdStudioLoader.getModelFile(fileName));
        } catch (FileNotFoundException e) {
            bdStudioLoader.sendErrorMessage(player, "The specified file was not found.");
            return false;
        } catch (IOException e) {
            bdStudioLoader.sendErrorMessage(player, "Could not load the file, see console.");
            bdStudioLoader.getLogger().severe("Error loading file: " + e.getMessage());
            return false;
        }

        BlockDisplay spawnedModel = bdModel.spawn(bdStudioLoader.getModelNameKey(), location, new Matrix4f());
        bdStudioLoader.sendPrimaryMessage(player, "Spawned \"" + fileName + "\"");

        modelEditManager.startPlacing(player, spawnedModel);
        return true;
    }

    @Override
    public List<String> onTabComplete(CommandSender sender, Command command, String label, String[] args) {
        if (args.length != 1) {
            return List.of();
        }
        return bdStudioLoader.getModels().stream()
                .filter(s -> s.startsWith(args[0]))
                .collect(Collectors.toList());
    }

}
