package de.petomka.bdstudioloader.commands;

import de.petomka.bdstudioloader.BDStudioLoader;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabExecutor;
import org.bukkit.entity.Player;

import java.util.List;

public class BDWandCommand implements TabExecutor {

    private final BDStudioLoader bdStudioLoader;

    public BDWandCommand(BDStudioLoader bdStudioLoader) {
        this.bdStudioLoader = bdStudioLoader;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (!(sender instanceof Player player)) {
            sender.sendMessage("This is a player command.");
            return true;
        }
        if (player.getInventory().firstEmpty() < 0) {
            bdStudioLoader.sendErrorMessage(player, "Cannot give you the wand because your inventory is full.");
            return true;
        }
        player.getInventory().addItem(BDStudioLoader.BD_WAND);
        return true;
    }

    @Override
    public List<String> onTabComplete(CommandSender sender, Command command, String label, String[] args) {
        return List.of();
    }

}
