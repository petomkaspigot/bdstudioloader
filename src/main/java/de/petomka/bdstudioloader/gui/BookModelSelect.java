package de.petomka.bdstudioloader.gui;

import de.petomka.bdstudioloader.BDStudioLoader;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.BookMeta;

import java.util.ArrayList;
import java.util.List;

public class BookModelSelect {

    private record PageInfo(int entries, BaseComponent[] content) {}

    public static void open(BDStudioLoader bdStudioLoader, Player player) {
        ItemStack bookItem = new ItemStack(Material.WRITTEN_BOOK);
        BookMeta meta = (BookMeta) bookItem.getItemMeta();
        assert meta != null;

        String author = "unknown";
        List<String> pluginAuthors = bdStudioLoader.getDescription().getAuthors();
        if (!pluginAuthors.isEmpty()) {
            author = pluginAuthors.get(0);
        }
        meta.setTitle("BSL Model Select");
        meta.setAuthor(author);

        meta.setGeneration(BookMeta.Generation.COPY_OF_COPY);

        int skip = 0;
        List<BaseComponent[]> pages = new ArrayList<>();
        List<String> files = bdStudioLoader.getModels();
        while (skip < files.size()) {
            PageInfo pageInfo = getBookPage(files, skip);
            pages.add(pageInfo.content);
            skip += pageInfo.entries;
        }

        meta.spigot().setPages(pages);
        bookItem.setItemMeta(meta);
        player.openBook(bookItem);
    }

    private static PageInfo getBookPage(List<String> filenames, int skip) {
        ComponentBuilder builder = new ComponentBuilder();
        int lineCount = 0;
        if (skip == 0) {
            builder.append("Select model\n\n").underlined(true).color(BDStudioLoader.MAIN_COLOR);
            lineCount = 2;
        }

        int entryCount = 0;
        for (int i = skip; i < filenames.size() && lineCount < 12; ++i) {
            ComponentBuilder entryBuilder = new ComponentBuilder()
                    .append(filenames.get(i) + "\n")
                    .underlined(false)
                    .color(ChatColor.BLACK)
                    .event(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/bdload " + filenames.get(i)));
            builder.append(entryBuilder.create());
            ++lineCount;
            ++entryCount;
        }

        return new BookModelSelect.PageInfo(entryCount, builder.create());
    }

}
