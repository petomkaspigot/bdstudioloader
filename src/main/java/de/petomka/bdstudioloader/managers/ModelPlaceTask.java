package de.petomka.bdstudioloader.managers;

import de.petomka.bdstudioloader.BDStudioLoader;
import de.petomka.bdstudioloader.scoreboard.ScoreboardDisplay;
import org.bukkit.Location;
import org.bukkit.entity.BlockDisplay;
import org.bukkit.entity.Display;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.persistence.PersistentDataType;
import org.bukkit.util.RayTraceResult;

import java.util.ArrayList;
import java.util.List;

public class ModelPlaceTask extends ModelTask {

    private final BDStudioLoader bdStudioLoader;
    private final Player player;
    private final BlockDisplay model;

    private final List<Entity> modelEntities = new ArrayList<>();

    private float extraRotation = 0;

    private int gridSizeIndex = 0;

    public static double roundToNearestMultiple(double a, double b) {
        if (b == 0f) {
            return a;
        }
        // The Math.round() function rounds to the nearest integer.
        return Math.round(a / b) * b;
    }

    public ModelPlaceTask(BDStudioLoader bdStudioLoader, Player player, BlockDisplay model, ScoreboardDisplay scoreboardDisplay) {
        super(scoreboardDisplay);
        this.bdStudioLoader = bdStudioLoader;
        this.player = player;
        this.model = model;

        modelEntities.addAll(model.getPassengers());

        modelEntities.stream()
                .filter(e -> e instanceof Display)
                .map(e -> (Display) e)
                .forEach(e -> e.setTeleportDuration(1));

        scoreboardDisplay.setShowDeleteInfo(false);
        scoreboardDisplay.activate();
    }

    @Override
    public void run() {
        double gridSize = 0;
        if (gridSizeIndex < BDStudioLoader.GRID_SIZES.length) {
            gridSize = BDStudioLoader.GRID_SIZES[gridSizeIndex];
        }
        scoreboardDisplay.updatePlacement(this.model, modelEntities.size(), gridSize);

        RayTraceResult rayTraceResult = player.rayTraceBlocks(BDStudioLoader.PLACE_DISTANCE);
        if (rayTraceResult == null) {
            return;
        }
        Location location = rayTraceResult.getHitPosition().toLocation(player.getWorld());
        location.setPitch(0);
        location.setYaw(extraRotation);

        if (gridSize > 0) {
            location.setX(roundToNearestMultiple(location.getX(), gridSize));
            location.setY(roundToNearestMultiple(location.getY(), gridSize));
            location.setZ(roundToNearestMultiple(location.getZ(), gridSize));
        }

        model.teleport(location);
        for (Entity passenger : modelEntities) {
            passenger.teleport(location);
        }
    }

    @Override
    public void finish() {
        long[] uuids = new long[modelEntities.size() * 2];
        int index = 0;

        for (Entity passenger : modelEntities) {
            model.addPassenger(passenger);

            uuids[index] = passenger.getUniqueId().getMostSignificantBits();
            uuids[index + 1] = passenger.getUniqueId().getLeastSignificantBits();

            index += 2;
        }

        model.getPersistentDataContainer().set(
                bdStudioLoader.getModelEntitiesKey(),
                PersistentDataType.LONG_ARRAY,
                uuids
        );
        model.getPersistentDataContainer().set(
                bdStudioLoader.getModelRotationKey(),
                PersistentDataType.FLOAT,
                extraRotation
        );

        bdStudioLoader.sendPrimaryMessage(player, "Placed the model!");
        scoreboardDisplay.deactivate();
    }

    public void adjustRotation(float delta) {
        this.extraRotation += delta;
    }

    public void largerGrid() {
        this.gridSizeIndex = Math.min(this.gridSizeIndex + 1, BDStudioLoader.GRID_SIZES.length - 1);
    }

    public void smallerGrid() {
        this.gridSizeIndex = Math.max(this.gridSizeIndex - 1, 0);
    }

}
