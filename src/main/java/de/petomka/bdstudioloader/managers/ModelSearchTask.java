package de.petomka.bdstudioloader.managers;

import de.petomka.bdstudioloader.BDStudioLoader;
import de.petomka.bdstudioloader.model.BDModel;
import de.petomka.bdstudioloader.scoreboard.ScoreboardDisplay;
import org.bukkit.Color;
import org.bukkit.entity.*;
import org.bukkit.persistence.PersistentDataType;
import org.bukkit.util.Vector;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class ModelSearchTask extends ModelTask {

    private static final Color EDIT_GLOW_COLOR = Color.fromRGB(
            BDStudioLoader.MAIN_COLOR.getColor().getRGB() & 0x00FFFFFF
    );

    private static final Color DELETE_GLOW_COLOR = Color.fromRGB(
            BDStudioLoader.ERROR_COLOR.getColor().getRGB() & 0x00FFFFFF
    );

    private final Player player;
    private final ModelEditManager modelEditManager;
    private final BDStudioLoader bdStudioLoader;

    private BlockDisplay lastModel;
    private List<Display> modelEntities = new ArrayList<>();

    private boolean isDeleting = false;

    public ModelSearchTask(Player player, ModelEditManager modelEditManager, BDStudioLoader bdStudioLoader, ScoreboardDisplay scoreboardDisplay) {
        super(scoreboardDisplay);
        this.player = player;
        this.modelEditManager = modelEditManager;
        this.bdStudioLoader = bdStudioLoader;

        this.scoreboardDisplay.activate();

        setDeleting(false);
    }

    @Override
    public void run() {
        this.scoreboardDisplay.updateSearch(this.lastModel, this.modelEntities.size());

        var nearbyEntities = player.getWorld().getNearbyEntities(player.getLocation(),
                BDStudioLoader.PLACE_DISTANCE,
                BDStudioLoader.PLACE_DISTANCE,
                BDStudioLoader.PLACE_DISTANCE
        );

        List<BlockDisplay> candidates = new ArrayList<>();
        for (Entity nearby : nearbyEntities) {
            if (nearby.getType() != EntityType.BLOCK_DISPLAY) {
                continue;
            }
            if (!nearby.getPersistentDataContainer().has(bdStudioLoader.getModelEntitiesKey(), PersistentDataType.LONG_ARRAY)) {
                continue;
            }
            if (!(nearby instanceof BlockDisplay model)) {
                continue;
            }
            candidates.add(model);
        }

        if (candidates.isEmpty()) {
            if (this.lastModel != null) {
                setGlowing(lastModel, EDIT_GLOW_COLOR, false);
                this.lastModel = null;
                this.modelEntities.clear();
            }
            return;
        }

        BlockDisplay best = candidates.get(0);
        float bestAngle = 1e38f;
        for (BlockDisplay candidate : candidates) {
            float angle = viewAngle(player, candidate);
            if (angle < bestAngle) {
                best = candidate;
                bestAngle = angle;
            }
        }

        if (lastModel != null && !lastModel.equals(best)) {
            setGlowing(lastModel, EDIT_GLOW_COLOR, false);
            this.lastModel = null;
            this.modelEntities.clear();
        }

        if (bestAngle > 0.1) {
            if (lastModel != null) {
                setGlowing(lastModel, EDIT_GLOW_COLOR, false);
                this.lastModel = null;
                this.modelEntities.clear();
            }
            return;
        }

        if (lastModel != null && lastModel.equals(best)) {
            return;
        }

        this.lastModel = best;
        this.modelEntities = setGlowing(best, isDeleting ? DELETE_GLOW_COLOR : EDIT_GLOW_COLOR, true);

        this.scoreboardDisplay.updateSearch(best, this.modelEntities.size());
    }

    private List<Display> setGlowing(BlockDisplay model, Color glowColor, boolean glowing) {
        model.setGlowing(glowing);
        model.setGlowColorOverride(glowColor);

        List<Display> entities = BDModel.getModelDisplayEntities(bdStudioLoader, player, model);
        for (Display display : entities) {
            display.setGlowing(glowing);
            display.setGlowColorOverride(glowColor);
        }

        return entities;
    }

    private float viewAngle(Player player, BlockDisplay display) {
        Vector p_to_d = display.getLocation().toVector().subtract(player.getEyeLocation().toVector());
        return player.getLocation().getDirection().angle(p_to_d);
    }

    @Override
    public void finish() {
        if (this.lastModel != null) {
            setGlowing(this.lastModel, EDIT_GLOW_COLOR, false);
        }
        this.scoreboardDisplay.deactivate();
    }

    public Optional<BlockDisplay> selectModel() {
        return Optional.ofNullable(this.lastModel);
    }

    public void setDeleting(boolean deleting) {
        boolean change = this.isDeleting != deleting;
        this.isDeleting = deleting;
        if (change && lastModel != null) {
            this.setGlowing(this.lastModel, isDeleting ? DELETE_GLOW_COLOR : EDIT_GLOW_COLOR, true);
        }
        this.scoreboardDisplay.setShowDeleteInfo(deleting);
        if (change && isDeleting) {
            this.scoreboardDisplay.notifyDeleting();
        }
        if (change && !isDeleting) {
            this.scoreboardDisplay.notifyEditing();
        }
    }

    public boolean isDeleting() {
        return isDeleting;
    }

    public void resetTarget() {
        this.lastModel = null;
        this.modelEntities.clear();
    }

}
