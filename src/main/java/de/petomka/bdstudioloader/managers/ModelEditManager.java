package de.petomka.bdstudioloader.managers;

import de.petomka.bdstudioloader.BDStudioLoader;
import de.petomka.bdstudioloader.gui.BookModelSelect;
import de.petomka.bdstudioloader.model.BDModel;
import de.petomka.bdstudioloader.scoreboard.ScoreboardDisplay;
import org.bukkit.Bukkit;
import org.bukkit.entity.BlockDisplay;
import org.bukkit.entity.Display;
import org.bukkit.entity.Player;
import org.bukkit.persistence.PersistentDataContainer;
import org.bukkit.persistence.PersistentDataType;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.BiFunction;
import java.util.function.Function;

public class ModelEditManager {

    private final Map<Player, ModelTask> activePlayerTasks = new HashMap<>();

    private final BDStudioLoader bdStudioLoader;

    public ModelEditManager(BDStudioLoader bdStudioLoader) {
        this.bdStudioLoader = bdStudioLoader;
    }

    private <T extends ModelTask> BiFunction<Player, ModelTask, T> computeNewTask(Function<ScoreboardDisplay, T> modelTaskSupplier) {
        return (player, oldTask) -> {
            ScoreboardDisplay display = new ScoreboardDisplay(bdStudioLoader, player);
            if (oldTask != null) {
                oldTask.finish();
                oldTask.cancel();
                display = oldTask.getScoreboardDisplay();
            }
            return modelTaskSupplier.apply(display);
        };
    }

    public void startPlacing(Player player, BlockDisplay model) {
        activePlayerTasks
                .compute(player, computeNewTask(display -> new ModelPlaceTask(bdStudioLoader, player, model, display)))
                .runTaskTimer(bdStudioLoader, 1L, 1L);
    }

    public void startSearching(Player player) {
        activePlayerTasks
                .compute(player, computeNewTask(display -> new ModelSearchTask(player, this, bdStudioLoader, display)))
                .runTaskTimer(bdStudioLoader, 1L, 1L);
    }

    public void stopSearching(Player player) {
        ModelTask task = activePlayerTasks.getOrDefault(player, null);
        if (!(task instanceof ModelSearchTask searchTask)) {
            return;
        }
        searchTask.finish();
        searchTask.cancel();
        activePlayerTasks.remove(player);
    }

    public boolean isBusy(Player player) {
        return activePlayerTasks.containsKey(player);
    }

    public boolean isPlacing(Player player) {
        return activePlayerTasks.getOrDefault(player, null) instanceof ModelPlaceTask;
    }

    public boolean isSearching(Player player) {
        return activePlayerTasks.getOrDefault(player, null) instanceof ModelSearchTask;
    }

    public void handleLeftClick(Player player) {
        ModelTask task = activePlayerTasks.getOrDefault(player, null);
        if (!(task instanceof ModelPlaceTask placeTask)) {
            return;
        }

        if (!player.hasPermission(bdStudioLoader.getWandPermission())) {
            return;
        }
        if (player.getInventory().getItemInMainHand().isSimilar(BDStudioLoader.BD_WAND)) {
            startSearching(player);
        } else {
            placeTask.finish();
            placeTask.cancel();
            activePlayerTasks.remove(player);
        }
    }

    public void handleRightClick(Player player) {
        ModelTask task = activePlayerTasks.getOrDefault(player, null);
        if (!(task instanceof ModelSearchTask searchTask)) {
            return;
        }
        Optional<BlockDisplay> optionalModel = searchTask.selectModel();
        if (optionalModel.isEmpty()) {
            if (player.isSneaking()) {
                BookModelSelect.open(bdStudioLoader, player);
            } else {
                bdStudioLoader.sendErrorMessage(player, "You have no model selected.");
            }
            return;
        }
        BlockDisplay model = optionalModel.get();
        if (searchTask.isDeleting()) {
            List<Display> entities = BDModel.getModelDisplayEntities(bdStudioLoader, player, model);
            for (Display display : entities) {
                display.remove();
            }
            model.remove();
            bdStudioLoader.sendPrimaryMessage(player, "The model was removed.");
            searchTask.resetTarget();
            return;
        }

        PersistentDataContainer dataContainer = model.getPersistentDataContainer();

        float rotation = 0;
        if (dataContainer.has(bdStudioLoader.getModelRotationKey(), PersistentDataType.FLOAT)) {
            Float rotationF = dataContainer.get(bdStudioLoader.getModelRotationKey(), PersistentDataType.FLOAT);
            if (rotationF == null) {
                bdStudioLoader.getLogger().warning("Invalid rotation (null) stored in model " + model.getUniqueId());
            } else {
                rotation = rotationF;
                if (rotation != rotation) { // Nan != NaN, +-Inf != +-Inf
                    bdStudioLoader.getLogger().warning("Invalid rotation (" + rotation + ") stored in model " + model.getUniqueId());
                    rotation = 0;
                }
            }
        }

        bdStudioLoader.sendPrimaryMessage(player, "Started editing the model.");
        startPlacing(player, model);
        handleRotation(player, rotation);
    }

    public void handleHandSwap(Player player) {
        ModelTask task = activePlayerTasks.getOrDefault(player, null);
        if (!(task instanceof ModelSearchTask searchTask)) {
            return;
        }
        searchTask.setDeleting(!searchTask.isDeleting());
    }

    public void handleQuit(Player player) {
        ModelTask task = activePlayerTasks.remove(player);
        if (task == null) {
            return;
        }
        task.finish();
        task.cancel();
    }

    public void handleRotation(Player player, float delta) {
        ModelTask task = activePlayerTasks.getOrDefault(player, null);
        if (!(task instanceof ModelPlaceTask modelPlaceTask)) {
            return;
        }
        modelPlaceTask.adjustRotation(delta);
    }

    public void handleGrid(Player player, boolean increase) {
        ModelTask task = activePlayerTasks.getOrDefault(player, null);
        if (!(task instanceof ModelPlaceTask modelPlaceTask)) {
            return;
        }
        if (increase) {
            modelPlaceTask.largerGrid();
        } else {
            modelPlaceTask.smallerGrid();
        }
    }

    public void shutdownTasks() {
        for (ModelTask modelTask : activePlayerTasks.values()) {
            modelTask.finish();
            modelTask.cancel();
        }
        activePlayerTasks.clear();
    }

}
