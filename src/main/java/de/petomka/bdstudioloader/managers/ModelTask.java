package de.petomka.bdstudioloader.managers;

import de.petomka.bdstudioloader.scoreboard.ScoreboardDisplay;
import org.bukkit.scheduler.BukkitRunnable;

public abstract class ModelTask extends BukkitRunnable {

    protected final ScoreboardDisplay scoreboardDisplay;

    protected ModelTask(ScoreboardDisplay scoreboardDisplay) {
        this.scoreboardDisplay = scoreboardDisplay;
    }

    public abstract void finish();

    public ScoreboardDisplay getScoreboardDisplay() {
        return this.scoreboardDisplay;
    }

}
