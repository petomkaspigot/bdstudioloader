package de.petomka.bdstudioloader;

import de.petomka.bdstudioloader.commands.BDLoadCommand;
import de.petomka.bdstudioloader.commands.BDStudioLoaderCommand;
import de.petomka.bdstudioloader.commands.BDWandCommand;
import de.petomka.bdstudioloader.listener.PlayerListener;
import de.petomka.bdstudioloader.managers.ModelEditManager;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.hover.content.Text;
import org.bstats.bukkit.Metrics;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.NamespacedKey;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.PluginCommand;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.awt.*;
import java.io.*;
import java.util.Arrays;
import java.util.List;
import java.util.function.Supplier;
import java.util.stream.Collectors;

public class BDStudioLoader extends JavaPlugin {

    private static final int BSTATS_PLUGIN_ID = 20255;

    private static final String[] EMBEDDED_MODELS = {
            "Computer.bdstudio",
            "Grandfather Clock.bdstudio",
            "Piano.bdstudio"
    };

    public static final ChatColor DARK_ACCENT_COLOR = ChatColor.of(new Color(0x0288d1));
    public static final ChatColor MAIN_COLOR = ChatColor.of(new Color(0x4fc3f7));
    public static final ChatColor SECONDARY_COLOR = ChatColor.of(new Color(0xe0e0e0));
    public static final ChatColor ERROR_COLOR = ChatColor.of(new Color(0xe57373));
    public static final double PLACE_DISTANCE = 5;
    public static final double[] GRID_SIZES = {
            0,
            1. / 16.,
            1 / 8.,
            1 / 4.,
            1 / 2.,
            1
    };

    private static final String MODEL_DIR = "models";

    public static final ItemStack BD_WAND = new ItemStack(Material.ECHO_SHARD);

    static {
        ItemMeta meta = BD_WAND.getItemMeta();
        assert meta != null;
        meta.setDisplayName(BaseComponent.toLegacyText(
                new ComponentBuilder()
                        .append("BDStudioWand").color(MAIN_COLOR)
                        .create()
        ));
        meta.addEnchant(Enchantment.ARROW_INFINITE, 1, true);
        meta.addItemFlags(ItemFlag.HIDE_ENCHANTS);
        BD_WAND.setItemMeta(meta);
    }


    private BaseComponent[] chatPrefix;
    private final NamespacedKey modelEntitiesKey = new NamespacedKey(this, "model_entities");
    private final NamespacedKey modelRotationKey = new NamespacedKey(this, "model_rotation");
    private final NamespacedKey modelNameKey = new NamespacedKey(this, "model_name");

    private final ModelEditManager modelEditManager = new ModelEditManager(this);

    private String wandPermission;

    @Override
    public void onEnable() {
        chatPrefix = new ComponentBuilder()
                .append(new ComponentBuilder().append("[")
                        .color(DARK_ACCENT_COLOR)
                        .append("BSL").color(MAIN_COLOR)
                        .event(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new Text(
                                new ComponentBuilder()
                                        .append(getDescription().getName())
                                        .append("\n")
                                        .append("version ")
                                        .append(getDescription().getVersion())
                                        .append("\n")
                                        .append("by ")
                                        .append(String.join(", ", getDescription().getAuthors()))
                                        .create()
                        )))
                        .append("] ").color(DARK_ACCENT_COLOR)
                        .event((HoverEvent) null)
                        .create()
                )
                .create();

        PluginCommand wandCommand = getCommand("bdstudiowand");
        if (wandCommand != null) {
            wandPermission = wandCommand.getPermission();
        }

        registerCommands();
        registerEvents();

        int modelSize = getModels().size(); // to create directories
        getLogger().info("Found " + modelSize + " models in model directory!");

        new Metrics(this, BSTATS_PLUGIN_ID);
    }

    @Override
    public void onDisable() {
        modelEditManager.shutdownTasks();
    }

    private void registerCommand(String commandName, Supplier<CommandExecutor> executorSupplier) {
        PluginCommand command = this.getCommand(commandName);
        if (command == null) {
            getLogger().severe("Could not load command \"" + commandName + "\"");
            return;
        }
        command.setExecutor(executorSupplier.get());
    }

    private void registerCommands() {
        registerCommand("bdstudioloader", () -> new BDStudioLoaderCommand(this));
        registerCommand("bdstudioload", () -> new BDLoadCommand(this, modelEditManager));
        registerCommand("bdstudiowand", () -> new BDWandCommand(this));
    }

    private void registerEvents() {
        PluginManager manager = Bukkit.getPluginManager();
        manager.registerEvents(new PlayerListener(this, modelEditManager), this);
    }

    public File getModelDir() {
        return new File(this.getDataFolder(), MODEL_DIR);
    }

    public File getModelFile(String fileName) {
        return new File(getModelDir(), fileName);
    }

    public List<String> getModels() {
        File modelDir = getModelDir();
        if (!modelDir.exists()) {
            if (modelDir.mkdirs()) {
                getLogger().info("created model directory");

                for (String embeddedModel : EMBEDDED_MODELS) {
                    InputStream modelStream = getResource(embeddedModel);
                    if (modelStream == null) {
                        getLogger().info("Could not find embedded resource \"" + embeddedModel + "\"");
                        continue;
                    }
                    File file = new File(getModelDir(), embeddedModel);

                    try (BufferedOutputStream outputStream = new BufferedOutputStream(new FileOutputStream(file))) {
                        byte[] buffer = new byte[1024];
                        int lengthRead;
                        while ((lengthRead = modelStream.read(buffer)) != -1) {
                            outputStream.write(buffer, 0, lengthRead);
                        }
                        getLogger().info("Created resource \"" + embeddedModel + "\"!");
                    } catch (IOException e) {
                        getLogger().info("Error while copying the embedded resource \"" + embeddedModel + "\": " + e.getMessage());
                    }
                }
            }
        }
        File[] files = modelDir.listFiles();
        if (files == null) {
            getLogger().warning("could not read files in model dir");
            return List.of();
        }
        return Arrays.stream(files)
                .filter(File::isFile)
                .map(File::getName)
                .collect(Collectors.toList());
    }

    public NamespacedKey getModelEntitiesKey() {
        return modelEntitiesKey;
    }

    public NamespacedKey getModelRotationKey() {
        return modelRotationKey;
    }

    public NamespacedKey getModelNameKey() {
        return modelNameKey;
    }

    public BaseComponent[] buildMessage(@Nonnull String message, @Nullable ChatColor messageColor) {
        ChatColor useColor = messageColor;
        if (useColor == null) {
            useColor = MAIN_COLOR;
        }
        return new ComponentBuilder()
                .append(chatPrefix)
                .event((HoverEvent) null)
                .append(message).color(useColor)
                .create();
    }

    public void sendMessage(Player player, String message, @Nullable ChatColor messageColor) {
        player.spigot().sendMessage(buildMessage(message, messageColor));
    }

    public void sendErrorMessage(Player player, String errorMessage) {
        sendMessage(player, errorMessage, ERROR_COLOR);
    }

    public void sendPrimaryMessage(Player player, String primaryMessage) {
        sendMessage(player, primaryMessage, MAIN_COLOR);
    }

    public String getWandPermission() {
        return wandPermission;
    }

}
