package de.petomka.bdstudioloader.scoreboard;

import de.petomka.bdstudioloader.BDStudioLoader;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.entity.BlockDisplay;
import org.bukkit.entity.Player;
import org.bukkit.persistence.PersistentDataContainer;
import org.bukkit.persistence.PersistentDataType;
import org.bukkit.scoreboard.*;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.awt.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.function.Consumer;

public class ScoreboardDisplay {

    private static final net.md_5.bungee.api.ChatColor AXIS_X_COLOR = net.md_5.bungee.api.ChatColor.of(new Color(0xFF0000));
    private static final net.md_5.bungee.api.ChatColor AXIS_Y_COLOR = net.md_5.bungee.api.ChatColor.of(new Color(0x00FF00));
    private static final net.md_5.bungee.api.ChatColor AXIS_Z_COLOR = net.md_5.bungee.api.ChatColor.of(new Color(0x7F7FFF));

    private static String buildScoreboardTitle(net.md_5.bungee.api.ChatColor color) {
        return BaseComponent.toLegacyText(new ComponentBuilder()
                .append("BSL").color(color).bold(true)
                .create());
    }

    private final BDStudioLoader bdStudioLoader;
    private final Player player;

    private Scoreboard scoreboard;
    private Objective objective;

    private Scoreboard previousBoard;

    private boolean showDeleteInfo = false;

    public ScoreboardDisplay(BDStudioLoader bdStudioLoader, Player player) {
        this.bdStudioLoader = bdStudioLoader;
        this.player = player;
        this.scoreboard = Bukkit.getScoreboardManager().getNewScoreboard();
        this.objective = scoreboard.registerNewObjective(
                "bsl",
                Criteria.DUMMY,
                buildScoreboardTitle(BDStudioLoader.MAIN_COLOR),
                RenderType.INTEGER
        );
        setShowDeleteInfo(this.showDeleteInfo); // update title
        this.objective.setDisplaySlot(DisplaySlot.SIDEBAR);
    }

    public void activate() {
        this.previousBoard = player.getScoreboard();
        if (player.getScoreboard().equals(Bukkit.getScoreboardManager().getMainScoreboard())) {
            player.setScoreboard(this.scoreboard);
        } else {
            bdStudioLoader.sendErrorMessage(player, "Did not activate scoreboard as it appears another scoreboard is active.");
        }
    }

    public void deactivate() {
        player.setScoreboard(previousBoard);
    }

    private void updatePreviousBoard() {
        if (!player.getScoreboard().equals(this.scoreboard)) {
            this.previousBoard = player.getScoreboard();
        }
    }

    private String getModelName(BlockDisplay model) {
        String modelName = "unknown model";
        PersistentDataContainer dataContainer = model.getPersistentDataContainer();
        if (dataContainer.has(bdStudioLoader.getModelNameKey(), PersistentDataType.STRING)) {
            String storedName = dataContainer.get(bdStudioLoader.getModelNameKey(), PersistentDataType.STRING);
            if (storedName != null && !storedName.isEmpty()) {
                modelName = storedName;
            } else {
                bdStudioLoader.getLogger().warning("Model " + model.getUniqueId() + " has empty or null name stored");
            }
        }
        return modelName;
    }

    public void updateSearch(@Nullable BlockDisplay model, int entityCount) {
        updatePreviousBoard();

        final var currentColor = this.showDeleteInfo ? BDStudioLoader.ERROR_COLOR : BDStudioLoader.MAIN_COLOR;

        List<String> lines = new ArrayList<>();
        if (model == null) {
            addLine(lines, builder -> {
                builder.append("Looking for models...").color(BDStudioLoader.SECONDARY_COLOR);
            });
            lines.add("");
            addLine(lines, builder -> {
                builder.append("You are not looking").color(currentColor);
            });
            addLine(lines, builder -> {
                builder.append("at any model.").color(currentColor);
            });
        } else {
            addLine(lines, builder -> {
                builder.append("Found a model!").color(BDStudioLoader.SECONDARY_COLOR);
            });
            lines.add("");
            final String modelName = getModelName(model);

            addModelInfo(currentColor, model.getLocation(), entityCount, lines, modelName);
        }

        if (showDeleteInfo) {
            lines.add("");
            addLine(lines, builder -> {
                builder.append("Warning!").color(net.md_5.bungee.api.ChatColor.DARK_RED).bold(true);
            });
            addLine(lines, builder -> {
                builder.append("You are in delete mode!").color(BDStudioLoader.ERROR_COLOR);
            });
        }

        writeLines(lines);
    }

    public void updatePlacement(@Nonnull BlockDisplay model, int entityCount, double gridSize) {
        updatePreviousBoard();

        List<String> lines = new ArrayList<>();

        String modelName = getModelName(model);
        addLine(lines, builder -> {
            builder.append("Placing a model!").color(BDStudioLoader.SECONDARY_COLOR);
        });
        lines.add("");
        addModelInfo(BDStudioLoader.MAIN_COLOR, model.getLocation(), entityCount, lines, modelName);
        if (gridSize > 0) {
            lines.add("");
            addLine(lines, builder -> {
                builder.append("Grid size:").color(BDStudioLoader.MAIN_COLOR);
            });
            addLine(lines, builder -> {
                builder.append(String.format("%.4f", gridSize)).color(BDStudioLoader.SECONDARY_COLOR);
            });
        }

        writeLines(lines);
    }

    private void addModelInfo(net.md_5.bungee.api.ChatColor currentColor, Location location, int entityCount, List<String> lines, String modelName) {
        addLine(lines, builder -> {
            builder.append("Model name:").color(currentColor);
        });
        addLine(lines, builder -> {
            builder.append(modelName).color(BDStudioLoader.SECONDARY_COLOR);
        });
        lines.add("");
        addLine(lines, builder -> {
            builder.append("Entity count: ").color(currentColor);
        });
        addLine(lines, builder -> {
            builder.append(String.valueOf(entityCount)).color(BDStudioLoader.SECONDARY_COLOR);
        });
        lines.add("");
        addLine(lines, builder -> {
            builder.append("Model location: ").color(currentColor);
        });
        addLine(lines, builder -> {
            builder.append(String.format("%.4f", location.getX())).color(AXIS_X_COLOR);
        });
        addLine(lines, builder -> {
            builder.append(String.format("%.4f", location.getY())).color(AXIS_Y_COLOR);
        });
        addLine(lines, builder -> {
            builder.append(String.format("%.4f", location.getZ())).color(AXIS_Z_COLOR);
        });
    }

    private void addLine(List<String> lines, Consumer<ComponentBuilder> builder) {
        ComponentBuilder componentBuilder = new ComponentBuilder();
        builder.accept(componentBuilder);
        lines.add(BaseComponent.toLegacyText(componentBuilder.create()));
    }

    private void writeLines(List<String> lines) {
        Set<Team> unusedTeams = new HashSet<>(scoreboard.getTeams());

        int teamNumber = 0;
        for (String line : lines) {
            String teamName = "team_" + teamNumber;
            String teamEntry = ChatColor.values()[teamNumber] + "";
            Team team = scoreboard.getTeam(teamName);
            if (team == null) {
                team = scoreboard.registerNewTeam(teamName);
                team.addEntry(teamEntry);
            } else {
                unusedTeams.remove(team);
            }

            team.setPrefix(line);
            objective.getScore(teamEntry).setScore(lines.size() - teamNumber);

            ++teamNumber;
        }

        for (Team team : unusedTeams) {
            team.getEntries().forEach(scoreboard::resetScores);
            team.unregister();
        }
    }

    public void notifyDeleting() {
        player.sendTitle(
                BaseComponent.toLegacyText(new ComponentBuilder()
                        .append("Warning!").color(net.md_5.bungee.api.ChatColor.DARK_RED).bold(true)
                        .create()),
                BaseComponent.toLegacyText(new ComponentBuilder()
                        .append("Delete mode activated!").color(BDStudioLoader.ERROR_COLOR)
                        .create()),
                2,
                40,
                2
        );
    }

    public void notifyEditing() {
        player.sendTitle(
                BaseComponent.toLegacyText(new ComponentBuilder().create()),
                BaseComponent.toLegacyText(new ComponentBuilder()
                        .append("Delete mode deactivated!").color(BDStudioLoader.MAIN_COLOR)
                        .create()),
                2,
                40,
                2
        );
    }

    public void setShowDeleteInfo(boolean showDeleteInfo) {
        net.md_5.bungee.api.ChatColor color = showDeleteInfo ? BDStudioLoader.ERROR_COLOR : BDStudioLoader.MAIN_COLOR;
        this.objective.setDisplayName(buildScoreboardTitle(color));
        this.showDeleteInfo = showDeleteInfo;
    }

}
