package de.petomka.bdstudioloader.model;

import de.petomka.bdstudioloader.BDStudioLoader;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.NamespacedKey;
import org.bukkit.entity.BlockDisplay;
import org.bukkit.entity.Display;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.persistence.PersistentDataContainer;
import org.bukkit.persistence.PersistentDataType;
import org.joml.Matrix4f;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class BDModel {

    private String name;

    private List<BDComponent> components;

    public BDModel(String name, List<BDComponent> components) {
        this.name = name;
        this.components = components;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<BDComponent> getComponents() {
        return components;
    }

    public void setComponents(List<BDComponent> components) {
        this.components = components;
    }

    public BlockDisplay spawn(NamespacedKey modelNameKey, Location baseLocation, Matrix4f transformMatrix) {
        BlockDisplay baseEntity = baseLocation.getWorld().spawn(baseLocation, BlockDisplay.class);
        for (BDComponent component : components) {
            component.spawn(baseEntity, transformMatrix);
        }

        PersistentDataContainer dataContainer = baseEntity.getPersistentDataContainer();
        dataContainer.set(modelNameKey, PersistentDataType.STRING, this.name);

        return baseEntity;
    }

    public static List<UUID> getModelUUIDS(BDStudioLoader bdStudioLoader, Player player, BlockDisplay model) {
        PersistentDataContainer dataContainer = model.getPersistentDataContainer();
        if (!dataContainer.has(bdStudioLoader.getModelEntitiesKey(), PersistentDataType.LONG_ARRAY)) {
            return List.of();
        }

        long[] uuidBytes = dataContainer.get(bdStudioLoader.getModelEntitiesKey(), PersistentDataType.LONG_ARRAY);
        if (uuidBytes == null) {
            bdStudioLoader.getLogger().warning("UUID array of model " + model.getUniqueId() + " is null.");
            bdStudioLoader.sendErrorMessage(player, "The model data is missing. Cannot select.");
            return List.of();
        }
        if (uuidBytes.length % 2 != 0) {
            bdStudioLoader.getLogger().warning("UUID array of model " + model.getUniqueId() + " is uneven in length, is it damaged?");
            bdStudioLoader.sendErrorMessage(player, "The model data is damaged. Cannot select.");
            return List.of();
        }
        List<UUID> result = new ArrayList<>();
        for (int i = 0; i < uuidBytes.length; i += 2) {
            UUID current = new UUID(uuidBytes[i], uuidBytes[i + 1]);
            result.add(current);
        }
        return result;
    }

    public static List<Display> getModelDisplayEntities(BDStudioLoader bdStudioLoader, Player player, BlockDisplay model) {
        List<UUID> modelUUIDs = getModelUUIDS(bdStudioLoader, player, model);
        List<Display> result = new ArrayList<>();

        for (UUID current : modelUUIDs) {
            Entity entity = Bukkit.getEntity(current);
            if (entity == null) {
                bdStudioLoader.getLogger().warning("Invalid entity UUID in model " + model + ": " + current);
                continue;
            }
            if (!(entity instanceof Display display)) {
                bdStudioLoader.getLogger().warning("Entity with UUID " + current + " in model " + model + " is not a Display entity");
                continue;
            }
            result.add(display);
        }

        return result;
    }

}
