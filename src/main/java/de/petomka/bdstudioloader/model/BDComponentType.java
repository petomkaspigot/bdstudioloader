package de.petomka.bdstudioloader.model;

public enum BDComponentType {

    COLLECTION,
    BLOCK_DISPLAY,
    ITEM_DISPLAY,
    TEXT_DISPLAY

}
