package de.petomka.bdstudioloader.listener;

import de.petomka.bdstudioloader.BDStudioLoader;
import de.petomka.bdstudioloader.managers.ModelEditManager;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerItemHeldEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.event.player.PlayerSwapHandItemsEvent;
import org.bukkit.inventory.EquipmentSlot;
import org.bukkit.inventory.ItemStack;

public class PlayerListener implements Listener {

    private final BDStudioLoader bdStudioLoader;
    private final ModelEditManager modelEditManager;

    public PlayerListener(BDStudioLoader bdStudioLoader, ModelEditManager modelEditManager) {
        this.bdStudioLoader = bdStudioLoader;
        this.modelEditManager = modelEditManager;
    }

    @EventHandler
    public void onClick(PlayerInteractEvent event) {
        if (!modelEditManager.isBusy(event.getPlayer())) {
            return;
        }
        if (event.getHand() != EquipmentSlot.HAND) {
            return;
        }
        if (event.getAction() == Action.LEFT_CLICK_AIR || event.getAction() == Action.LEFT_CLICK_BLOCK) {
            modelEditManager.handleLeftClick(event.getPlayer());
            event.setCancelled(true);
        }
        if (event.getAction() == Action.RIGHT_CLICK_AIR || event.getAction() == Action.RIGHT_CLICK_BLOCK) {
            modelEditManager.handleRightClick(event.getPlayer());
            event.setCancelled(true);
        }
    }

    private static float getShortestPathDirection(int a, int b, int N) {
        // Normalize a and b to be within the range [0, N-1]
        a = ((a % N) + N) % N;
        b = ((b % N) + N) % N;

        // Calculate the difference modulo N
        int rightPath = (b - a + N) % N;
        int leftPath = (a - b + N) % N;

        // Determine which path is shorter
        if (rightPath <= leftPath) {
            return 1;
        } else {
            return -1;
        }
    }

    @EventHandler
    public void onSlotChange(PlayerItemHeldEvent event) {
        ItemStack newItem = event.getPlayer().getInventory().getItem(event.getNewSlot());
        if (checkWand(event.getPlayer(), newItem)) {
            modelEditManager.startSearching(event.getPlayer());
            return;
        } else {
            modelEditManager.stopSearching(event.getPlayer());
        }
        if (!modelEditManager.isBusy(event.getPlayer())) {
            return;
        }
        float delta = getShortestPathDirection(event.getPreviousSlot(), event.getNewSlot(), 9);
        if (event.getPlayer().isSneaking()) {
            modelEditManager.handleGrid(event.getPlayer(), delta > 0);
        } else {
            modelEditManager.handleRotation(event.getPlayer(), delta);
        }
        ItemStack oldItem = event.getPlayer().getInventory().getItemInMainHand();
        if (oldItem.isSimilar(BDStudioLoader.BD_WAND)) {
            event.setCancelled(true);
        }
    }

    @EventHandler
    public void onQuit(PlayerQuitEvent event) {
        modelEditManager.handleQuit(event.getPlayer());
    }

    @EventHandler
    public void onSwapHand(PlayerSwapHandItemsEvent event) {
        if (!modelEditManager.isSearching(event.getPlayer())) {
            return;
        }
        ItemStack item = event.getOffHandItem();
        if (item == null) {
            return;
        }
        if (!item.isSimilar(BDStudioLoader.BD_WAND)) {
            return;
        }
        modelEditManager.handleHandSwap(event.getPlayer());
        event.setCancelled(true);
    }

    private boolean checkWand(Player player, ItemStack newItem) {
        if (modelEditManager.isBusy(player)) {
            return false;
        }
        if (!player.hasPermission(bdStudioLoader.getWandPermission())) {
            return false;
        }
        if (newItem == null) {
            return false;
        }
        return newItem.isSimilar(BDStudioLoader.BD_WAND);
    }

}
