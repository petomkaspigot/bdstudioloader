plugins {
    id("java")
    id("com.github.johnrengelman.shadow") version "7.1.2" // Shadow plugin
}

val author = "petomka"
group = "de.petomka"
version = "1.0"

val replacements = mapOf("tokens" to mapOf(
    "author" to author,
    "version" to version
))

repositories {
    mavenCentral()
    maven("https://hub.spigotmc.org/nexus/content/repositories/snapshots/")
    maven("https://oss.sonatype.org/content/repositories/snapshots")
    maven("https://oss.sonatype.org/content/repositories/central")
}

dependencies {
    // Change 'compileOnly' to 'implementation' for dependencies to be included in the shadow JAR
    implementation("org.bstats:bstats-bukkit:3.0.2")

    compileOnly("org.spigotmc:spigot-api:1.20.2-R0.1-SNAPSHOT")
    testImplementation("org.spigotmc:spigot-api:1.20.2-R0.1-SNAPSHOT")

    testImplementation(platform("org.junit:junit-bom:5.9.1"))
    testImplementation("org.junit.jupiter:junit-jupiter")
}

tasks.test {
    useJUnitPlatform()
}

java {
    toolchain.languageVersion.set(JavaLanguageVersion.of(17))
}

tasks.processResources {
    filesMatching("**/*.yml") {
        filter(org.apache.tools.ant.filters.ReplaceTokens::class, replacements)
    }
}

tasks.shadowJar {
    // Configure the Shadow Jar task
    archiveFileName.set("${project.name}-${project.version}.jar")
    relocate("org.bstats", "de.petomka.bdstudioloader.bstats") // Relocate packages
}
